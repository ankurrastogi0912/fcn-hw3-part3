import socket
import sys
import json
import threading
from thread import *
import time


socket_list = []
client_socket_list = []
myRoutingTable = []

def fetchMyRoutingTable(host):
	f = open("./global_routing_table.json","r")
	routingObject = json.loads(f.read())
	return routingObject[host];

def fetchGlobalNeighbour(host):
	f = open("./neighbor_list.json","r")
	neighborObject = json.loads(f.read())
	return neighborObject[host];

def myServerIp(host):
	ipPortDetails = fetchHostIpDetails()
	return ipPortDetails[host]["ip"]

def myServerPort(host):
	ipPortDetails = fetchHostIpDetails()
	return ipPortDetails[host]["port"]

def fetchHostIpDetails():
	f = open("./host_ip_port.json","r")
	ipPortDetails = json.loads(f.read())
	return ipPortDetails

def fetchNeighborDetails(host):
	myNeighbors = fetchGlobalNeighbour(host)
	hostIpDetails = fetchHostIpDetails()

	for index, neighbor in enumerate(myNeighbors):
		tempDetails = hostIpDetails[neighbor]
		tempDetails["host"] = neighbor
		myNeighbors[index] = tempDetails

	return myNeighbors


# def threaded_client(conn):
#     conn.send('Welcome, type your info\n')

#     while True:
#         data = conn.recv(2048)
#         reply = 'Server output: '+ data
#         if not data:
#             break
#         conn.sendall(reply)
#     conn.close()

def socketServer(ip, port):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	try:
	    s.bind((ip, port))
	except socket.error as e:
	    print(str(e))

	s.listen(5)

	while True:
	    conn, addr = s.accept()
	    socket_list.append((conn,addr));

def startServerThread():
	ip = myServerIp(host)
	port = myServerPort(host)
	print("===============> Starting socket server at ip and port: " + str(ip) + " " + str(port))
	start_new_thread(socketServer,(ip, port))

def createSocketConnectionToNeighbors(host):
	myNeighbor = fetchNeighborDetails(host)
	print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ " + json.dumps(myNeighbor))
	for neighbor in myNeighbor:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((neighbor["ip"], neighbor["port"]))
		s.setblocking(0);
		client_socket_list.append(s)

def stringToJsonAdapter(routeStr):
	#H1!!@@!! R1$$R2$$0 @@@@R1$$R3$$5
	routeStr = routeStr.split("!!@@!!");
	client = routeStr[0];
	routeStr = routeStr[1];

	routeStr = routeStr.split("@@@@");

	jsonObj = {}
	jsonObj[client] = []
	
	for item in routeStr:
		item = item.split("$$")
		destination = item[0]
		next_hop = item[1]
		cost = item[2]
		temp = {}
		temp["destination"] = destination;
		temp["next_hop"] = next_hop;
		temp["cost"] = int(cost);
		jsonObj[client].append(temp);

	print("@@@@@@@@@@@@@@@@@@@@@@@@@@@: " + json.dumps(jsonObj));
	return jsonObj;

'''
"R1" : [
	{ "destination" : "R1", "cost" : 0, "next_hop" : "R1" },
	{ "destination" : "H1", "cost" : 2, "next_hop" : "H1" },
	{ "destination" : "R2", "cost" : 10, "next_hop" : "R2" },
	{ "destination" : "R3", "cost" : 6, "next_hop" : "R3" }
]
'''
def JsonToStringAdapter(routeJson):
	#H1!!@@!!R1$$R2$$0@@@@R1$$R3$$5
	routeStr = "";
	for item in routeJson:
		routeStr += str(item) + "!!@@!!";
		for dest in routeJson[item]:
			routeStr += str(dest["destination"]) + "$$" + str(dest["next_hop"]) + "$$" + str(dest["cost"]) + "@@@@";
		if(routeStr[-1:] == "@"):
			routeStr = routeStr[:-4]

	print("@@@@@@@@@@@@@@@@@@@@@@@@@@@: " + routeStr);
	return routeStr;

def sendRoutingTableToAllClients(host):
	global myRoutingTable
	for client_socket in client_socket_list:
		print("############################ sending data to host " + host)
		client_socket.send(JsonToStringAdapter({ host : myRoutingTable}) + "####@@@@####")

def startListeningForIncomingSockets(host):
	global myRoutingTable
	while 1:
		for conn, addr in socket_list:
			clientRoutingTable = conn.recv(25000)
			print("===============> Got response from one of the neighbor")
			print("===============> Neighbor routing table is: " + clientRoutingTable)
			print("===============> Executing bellman ford algorithm ")
			clientRoutingTable = clientRoutingTable.split("####@@@@####")
			

			# tempIndex = len(clientRoutingTable)
			
			# if(tempIndex > 10):
			# 	tempIndex = 10;
			
			# clientRoutingTable = clientRoutingTable[0:tempIndex]


			if(len(clientRoutingTable) > 0):
				clientRoutingTable = clientRoutingTable[:-1]
			
			for rTable in clientRoutingTable:
				rTable = stringToJsonAdapter(rTable);
				executeBellmanFordAlgorithm(host, rTable)
		time.sleep(5)

def executeBellmanFordAlgorithm(host, clientRoutingTable):
	global myRoutingTable

	routingTableUpdatedFlag = False

	print("===============> My routing table: " + json.dumps(myRoutingTable))
	print("===============> neighbor routing table: " + json.dumps(clientRoutingTable))
	if type(clientRoutingTable) is str:
		clientRoutingTable = json.loads(clientRoutingTable)
	if clientRoutingTable.keys() and len(clientRoutingTable.keys()) > 0 :
		client = list(clientRoutingTable.keys())[0]
		print("===============> neighbor " + client + " routing table: " + json.dumps(clientRoutingTable))

		for myPath in myRoutingTable:
			if myPath["destination"] == client:
				myCostToClient = myPath["cost"]

	#TODO: Can check whether client is a neighbor or not

	tempMyRoutingTable = {}
	tempClientRoutingTable = {}

	for path in clientRoutingTable[client]:
		tempClientRoutingTable[path["destination"]] = path

	for path in myRoutingTable:
		tempMyRoutingTable[path["destination"]] = path


	for dest in tempMyRoutingTable:
		if dest in tempClientRoutingTable:
			if (tempClientRoutingTable[dest]["cost"] + tempMyRoutingTable[client]["cost"]) < tempMyRoutingTable[dest]["cost"]:
				tempMyRoutingTable[dest]["cost"] = tempClientRoutingTable[dest]["cost"] + tempMyRoutingTable[client]["cost"];
				tempMyRoutingTable[dest]["next_hop"] = client
				routingTableUpdatedFlag = True

	for dest in tempClientRoutingTable:
		if dest not in tempMyRoutingTable:
			tempMyRoutingTable[dest] = tempClientRoutingTable[dest]
			tempMyRoutingTable[dest]["cost"] += tempMyRoutingTable[client]["cost"]
			tempMyRoutingTable[dest]["next_hop"] = client
			routingTableUpdatedFlag = True


	myRoutingTable = []

	for dest in tempMyRoutingTable:
		myRoutingTable.append(tempMyRoutingTable[dest])

	# for path in clientRoutingTable[client]:
	# 	for index, myPath in enumerate(myRoutingTable):
	# 		print("###################################")
	# 		print(path["destination"], myPath["destination"], myCostToClient + path["cost"], myPath["cost"])
	# 		print("###################################")
	# 		if(path["destination"] == myPath["destination"] and (myCostToClient + path["cost"]) < myPath["cost"]):
	# 			myRoutingTable[index]["cost"] = myCostToClient + path["cost"];
	# 			myRoutingTable[index]["next_hop"] = client
	# 			routingTableUpdatedFlag = True

	f = open("./file_"+host+".txt",'a');
	f.write(json.dumps(myRoutingTable))
	f.close();


	if routingTableUpdatedFlag == True:
		print("===============> My upated routing table: " + json.dumps(myRoutingTable))
		print("===============> Sending my new routing table to all the neighbours")
		sendRoutingTableToAllClients(host)



def init():
	global myRoutingTable
	myRoutingTable = fetchMyRoutingTable(host)
	print("===============> My routing Table " + json.dumps(myRoutingTable))
	neighbourList = fetchGlobalNeighbour(host)
	print("===============> My neighbor list " + json.dumps(neighbourList))
	startServerThread()
	print("===============> Going to sleep for 10 seconds")
	time.sleep(10)
	print("===============> Making socket connection to all the neighbors")
	createSocketConnectionToNeighbors(host)
	print("===============> Sending my routing table to all the neighbors")
	sendRoutingTableToAllClients(host);
	print("===============> Now starting to listen on all the incoming ports")
	startListeningForIncomingSockets(host)

if __name__ == "__main__":
	host = sys.argv[1]
	#internal variables

	print(host)

	init()